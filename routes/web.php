<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\PermisstionController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RequestController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Models\HistoryRequestModel;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('login', [AuthController::class, 'login'])->name('login');
Route::post('login', [AuthController::class, 'logon'])->name('logon');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');
Route::get('regiter', [AuthController::class, 'regiter'])->name('regiter');
Route::get('forgot-password', [AuthController::class, 'forgotPassword'])->name('forgot.password');
Route::get('first-login', [AuthController::class, 'firstLogin'])->name('first.login');
Route::put('first-logon', [AuthController::class, 'firstLogon'])->name('first.logon');
Route::post('forgot_password/check_email',[AuthController::class, 'checkEmail'])->name('check.email');
Route::get('changePassword',[AuthController::class, 'changePassword'])->name('change.password');
Route::put('do-changePassword',[AuthController::class, 'doChangePassword'])->name('do.change.password');

Route::delete('requests/delete/{id}', [RequestController::class,'delete'])->name('delete.request')->middleware(['auth']);
Route::resource('requests', RequestController::class)->middleware(['auth']);

Route::prefix('dashboard')->middleware(['auth','first.login'])->group(function () {
    Route::get('/',[AuthController::class, 'index'])->name('dashboard');
    Route::get('Danh-sach-nhan-vien', [UserController::class, 'list'])->name('users.list');
    Route::get('Danh-sach-nhan-vien/{id}', [UserController::class, 'infomation'])->name('users.info');
    Route::get('user-view-product', [ProductController::class, 'userView'])->name('user.view.product');
    Route::get('user-list-request', [RequestController::class, 'userList'])->name('user.list.request');
    Route::put('request-solved', [RequestController::class, 'requestSolved'])->name('request.solved');
    Route::put('request-export', [RequestController::class, 'requestExport'])->name('request.export');
    Route::put('request-return', [RequestController::class, 'requestReturn'])->name('request.return');
    Route::put('do-export', [RequestController::class, 'doExport'])->name('do.export');
    Route::put('do-return', [RequestController::class, 'doReturn'])->name('do.return');
    Route::put('request-refuse', [RequestController::class, 'requestRefuse'])->name('request.refuse');
    Route::resource('products', ProductController::class);
    Route::middleware(['can:is-admin'])->group(function () {
        Route::resource('categories', CategoryController::class);
        Route::resource('users', UserController::class);
        Route::resource('departments', DepartmentController::class);
        Route::resource('roles', RoleController::class);
        Route::resource('permisstions', PermisstionController::class)->only(['index', 'create' , 'store', 'destroy']);
        Route::resource('history-requestModels', HistoryRequestModel::class)->names('history.requests');
    }); 
});
