<?php

namespace App\Providers;
use Illuminate\Support\Facades\Gate;
use App\Helper\Policy;

// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // App\Models\DepartmentModel::class => App\Policies\DepartmentPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('is-admin', function($user){
            return $user->type === typeAdmin;
        });
        Gate::define('is-user', function($user){
            return $user->type === typeUser;
        });
        Gate::define('category.index', 'App\Policies\CategoriesPolicy@view');
        Gate::define('category.create', 'App\Policies\CategoriesPolicy@create');
        Gate::define('category.update', 'App\Policies\CategoriesPolicy@update');
        Gate::define('category.delete', 'App\Policies\CategoriesPolicy@delete');

        Gate::define('product.index', 'App\Policies\ProductPolicy@view');
        Gate::define('product.create', 'App\Policies\ProductPolicy@create');
        Gate::define('product.update', 'App\Policies\ProductPolicy@update');
        Gate::define('product.delete', 'App\Policies\ProductPolicy@delete');

        Gate::define('department.index', 'App\Policies\DepartmentPolicy@view');
        Gate::define('department.create', 'App\Policies\DepartmentPolicy@create');
        Gate::define('department.update', 'App\Policies\DepartmentPolicy@update');
        Gate::define('department.delete', 'App\Policies\DepartmentPolicy@delete');

        Gate::define('role.index', 'App\Policies\RolePolicy@view');
        Gate::define('role.create', 'App\Policies\RolePolicy@create');
        Gate::define('role.update', 'App\Policies\RolePolicy@update');
        Gate::define('role.delete', 'App\Policies\RolePolicy@delete');

        Gate::define('user.index', 'App\Policies\UserPolicy@view');
        Gate::define('user.create', 'App\Policies\UserPolicy@create');
        Gate::define('user.update', 'App\Policies\UserPolicy@update');
        Gate::define('user.delete', 'App\Policies\UserPolicy@delete');

        Gate::define('request.index', 'App\Policies\RequestPolicy@view');
        Gate::define('request.create', 'App\Policies\RequestPolicy@create');
        Gate::define('request.update', 'App\Policies\RequestPolicy@update');
        Gate::define('request.delete', 'App\Policies\RequestPolicy@delete');

        Gate::define('permisstion.index', 'App\Policies\PermisstionsPolicy@view');
        Gate::define('permisstion.create', 'App\Policies\PermisstionsPolicy@create');
        Gate::define('permisstion.update', 'App\Policies\PermisstionsPolicy@update');
        Gate::define('permisstion.delete', 'App\Policies\PermisstionsPolicy@delete');

    }
}
