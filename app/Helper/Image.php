<?php
namespace App\Helper;

class Image
{
    // $fodel: nơi lưu ảnh
    // $Slug: lấy tên ảnh theo slug truyền vào
    // $File : file ảnh 
    public function uploadImage($file, $folder, $slug=""){
        if($file){
            $path = 'uploads/'.$folder.'/';
            $new_name = $slug .'-'. rand(0, 99).'.'.$file->getClientOriginalExtension();
            $file->move($path, $new_name);
            return $new_name;
        }
        return redirect()->back()->with('erros','Lỗi chọn ảnh');
    }
   
    public function updateImage($file, $folder, $slug="",$old_name ){
        $this->delImage($folder,$old_name);
        $data= $this->uploadImage( $file, $folder, $slug);
        return $data;
    }
    
    public function delImage($folder,$name){
        if (!empty($name)) {
            $path_del = 'uploads/'.$folder.'/' . $name;
            if (file_exists($path_del)) {
                unlink($path_del); 
            }
        }
    }
}
