<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentModel extends Model
{
    use HasFactory;
    public $timestamps = false ;
    protected $table = 'departments';
    protected $fillable = [
        'name',
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'department_id', 'id');
    } 

    public function del($id)
    {
        static::where('id',$id)->delete();
        return true;
    }
}
