<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserModel;
use Illuminate\Support\Facades\Hash;

class ChangePasswordModel extends Model
{
    use HasFactory;
    protected $table = 'change_passwords';
    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'token'
    ];
    public function user(){
        return $this->belongsTo(UserModel::class, 'user_id', 'id');
    }

    public function createtoken($user) 
    {
        $token = csrf_token(); 
        static::create([
            'token' => $token,
            'user_id' => $user->id
        ]);
        return $token;
    }

    public function findByToken($token)
    {
       $item =  static::where('token', $token)->first(); 
        return $item;
    }

    public function doChangPassword ($request)
    {
        $item = static::where('token', $request->token)->first();
        $item->user()->update([
            'password' => Hash::make($request->password),
        ]);
        $item->delete();
    }
}
