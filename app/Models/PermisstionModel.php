<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermisstionModel extends Model
{
    use HasFactory;
    protected $table= 'permisstions';
    public $timestamps = false;
    protected $fillabale = [
        'name',
        'parent_id',
        'key'
    ];

    public function childs(){
        return $this->hasMany(PermisstionModel::class,'parent_id');
    }

    public function roles()
    {
        return $this->belongsToMany(PermisstionModel::class, 'permisstion_role', 'permisstion_id', 'role_id');
    }
    public function list()
    {
        $permisstions = static::with(['roles', 'childs'])->where('parent_id', 0)->get();
        return $permisstions;
    }
}
