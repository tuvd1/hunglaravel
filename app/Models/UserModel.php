<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\DepartmentModel;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use App\Mail\ResetPasswordMail;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Mail;

class UserModel extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = 'users';
    protected $fillable = [
        'name',
        'user',
        'email',
        'gender',
        'first_login',
        'phone',
        'type',
        'department_id',
        'address',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function setPasswordAttribute($value)
    // {
    //     $this->attributes['password'] = Hash::make($value);
    // }
    public function department()
    {
        return $this->belongsTo(DepartmentModel::class, 'department_id', 'id');
    } 

    public function changePasswords()
    {
        return $this->hasMany(ChangePasswordModel::class, 'user_id', 'id');
    }

    public function  requsets() 
    {
        return $this->hasMany(RequestModel::class, 'user_id', 'id');
    }

    public function roles()
    {
        return $this->belongsToMany(RoleModel::class, 'role_user', 'user_id' , 'role_id');
    }

    public function historys()
    {
        return $this->hasMany(HistoryRequestModel::class, 'admin_id', 'id');
    }

    public function scopeSearchUser($query)
    {
        if ($search = request()->search) {
            $query = $query->where('name', 'like', '%' . $search . '%');
        }
        return $query;
    }

    public function scopeDepartment($query)
    {
        if(isset(request()->department) && request()->department != null) {
            $query = $query->where('department_id',request()->department);     
        }
        return $query;
    }

    public function list()
    {
        $users = static::with(['department', 'requsets', 'roles'])->SearchUser()->Department()->paginate(20);
        return $users;
    }

    public function listUser()
    {
        $users = static::with(['department', 'requsets', 'roles'])->where('type', typeUser)->SearchUser()->Department()->paginate(20);
        return $users;
    }

    public function findById($id)
    {
        $user = static::with(['department', 'requsets', 'roles'])->find($id);
        return $user;
    }


    public function createUser($request)
    {
        $user=static::create([
            'name' => $request ->name,
            'user' => $request ->user,
            'email' => $request ->email,
            'gender' => $request ->gender,    
            'phone' => $request ->phone,
            'type' => $request ->type,
            'password' => Hash::make($request ->password),
            'department_id' => $request ->department_id,
            'address' => $request ->address,
            'first_login' => 0,
            ]);
        if ($request->type == typeAdmin && $request->role != null) {
                $user->roles->sync($request->role);
            }
    }

    public function updateUser($request, $id)
    {
        $user= static::find($id);
        $user->update([
            'name' => $request ->name,
            'user' => $request ->user,
            'email' => $request ->email,
            'gender' => $request ->gender,    
            'phone' => $request ->phone,
            'type' => $request ->type,
            'department_id' => $request ->department_id,
            'address' => $request ->address,
        ]);
        if($request->password){
            $this->changePassword($request, $id);
        }
        if ($request->type == typeAdmin) {
            $user->roles()->sync($request->role);
        } else {
            $user->roles()->detach();
        }
    }

    public function changePassword($request, $id){
        static::find($id)->update([
            'password' => Hash::make($request ->password)
        ]);
    }

    public function createToken($user){
        $token = csrf_token(); 
        $user::changePasswords()->create([
            'token' => $token
        ]);
        return $token;
    }

    public function deleteUser($id)
    {
        if($this::where('id',$id)->delete()) {
            return true;
        }
        return false;
    }
    
    public function checkEmail($emali)
    {
        $user = static::where('email', $emali)->first();
        return $user;
    }

    public function sendMail($user, $token)
    {
        $mailable = new ResetPasswordMail($user,  $token);
        Mail::to($user->email)->queue($mailable);
    }

    public function checkPemisstion($check){
        $roles=Auth::user()->roles;
        foreach($roles as $role){
            $permisstion = $role->permisstions; 
            if($permisstion->contains('key',$check)){
                return True;
            }
        }
        return false;
    }  
}
