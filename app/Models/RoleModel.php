<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleModel extends Model
{
    use HasFactory;
    protected $table = 'roles';
    protected $fillable = [
        'name',
    ];
    public $timestamps = false;

    public function users()
    {
        return $this->belongsToMany(UserModel::class, 'role_user', 'role_id', 'user_id');
    }
    public function permisstions()
    {
        return $this->belongsToMany(PermisstionModel::class, 'permisstion_role', 'role_id', 'permisstion_id');
    }

    public function createRole ($request)
    {
        $role= static::create([
            'name' => $request->name,
        ]);
        $role->permisstions()->sync($request->child);
    }

    public function list ()
    {
        $roles = static::with(['permisstions', 'users'])->get();
        return $roles;
    }
    public function updateRole ($request, $id)
    {   $role = static::find($id);
        $role->update([
            'name' => $request->name,
        ]);
        $role->permisstions()->sync($request->child);
    }

    public function deleteRole ($id)
    {
        static::where('id', $id)->delete();
    }

}
