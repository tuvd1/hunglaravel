<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class CategoryModel extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'Categories';
    protected $fillable = [
        'name',
        'slug'
    ];
    public function products()
    {
        return $this->hasMany(ProductModel::class, 'category_id', 'id');
    }

    public function list()
    {
        $categories = static::with(['products'])->orderby('id', 'Desc')->get();
        return  $categories;
    }

    public function createCate($request) 
    {
        static::create([
            'name' => $request->name,
            'slug' => Str::slug($request->name)
        ]);
    }

    public function updateCate($request, $id)
    {
        static::where('id', $id)->update([
            'name' => $request->name,
            'slug' => Str::slug($request->name)
        ]);
    }

    public function deleteCategory($id)
    {
        if(static::where('id',$id)->delete()){
            return true;
        }
        return false;
    }
    public function chartJs($categories)
    {
        foreach ($categories as $key => $cate) {
            $names[]= $cate->name;
            $count[] = $cate->products()->where('status', warehouse)->count();
            $count1[] = $cate->products()->where('status', borrow)->count();
        }
        $data=[
            'names' => $names,
            'count' => $count,
            'count1' => $count1,
        ];
        return $data;
    }
}
