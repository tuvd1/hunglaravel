<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class RequestModel extends Model
{
    use HasFactory;
    protected $table = "requests";
    public $timestamps = true;
    protected $fillable = [
        'user_id',
        'product_id',
        'status',
        'brorrowed_date',
        'date_off',
        'action'
    ];
    public function  user() 
    {
        return $this->belongsTo(UserModel::class, 'user_id', 'id');
    }

    public function  product() 
    {
        return $this->belongsTo(ProductModel::class, 'product_id', 'id');
    }

    public function  history() 
    {
        return $this->hasOne(HistoryRequestModel::class, 'request_id', 'id');
    }

    public function scopeSearch($query)
    {
        if ($search = request()->search) {
            $query = $query->where('name', 'like', '%' . $search . '%');
        }
        return $query;
    }
    public function scopeStatus($query)
    {
        if(isset(request()->status) && request()->status != null) {
            $query = $query->where('status',request()->status);     
        }
        return $query;
    }

    public function scopeAction($query)
    {
        if(isset(request()->action) && request()->action != null) {
            $query = $query->where('action',request()->action);     
        }
        return $query;
    }

    public function scopeAction1($query)
    {
        if(isset(request()->action1) && request()->action1 != null) {
            $query = $query->where('action',request()->action1);     
        }
        return $query;
    }

    public function scopeExAction1($query)
    {
        if(isset(request()->ex_action1) && request()->ex_action1 != null) {
            $query = $query->where('action',"!=",request()->ex_action1);     
        }
        return $query;
    }

    public function scopeExAction($query)
    {
        if(isset(request()->ex_action) && request()->ex_action != null) {
            $query = $query->where('action',"!=",request()->ex_action);     
        }
        return $query;
    }

    public function list() 
    {
        $requests= static::with(['product','user'])->orderby('id','desc')->action()->Search()->status()->exAction1()->exAction()->paginate(20);
        return $requests;
    }

    public function UserListRequest()
    {
        $requests= static::with(['product','user'])->orderby('id','desc')->status()->Search()->action()->exAction()->where('user_id', Auth::id())->paginate(20);
        return $requests;
    }

    public function findById ($id)
    { 
        $request = static::with(['product','user'])->where('id',$id)->where('user_id', Auth::id())->first();
        if(!empty($request)) {
            return $request;
        }
        return false ; 
    }

    public function deleteRequest ($id)
    {
        $request = static::find($id);
        if($request->user_id == Auth::id()) {
            $request->delete();
            return true;
        }
        return false;
    }

    public function createRequest($request)
    {
        static::create([
            'user_id' => Auth::id(),
            'product_id' => $request->product_id,
            'status' => 0,
            'brorrowed_date' => $request->brorrowed_date,
            'date_off' => $request -> date_off
        ]);
    }

    public function updateRequest($request, $id)
    {
        static::where('id', $id)->update([
            'brorrowed_date' => $request->brorrowed_date,
            'date_off' => $request -> date_off
        ]);
    }

    public function requestSolved($id)
    {
        static::where('id', $id)->update([
            'status' => requestSolved,
        ]);
    }

    public function requestRefuse($id)
    {
        static::where('id', $id)->update([
            'status' => requestRefuse,
            'action' => 0,
        ]);
    }

    public function requestExport($id)
    {
        static::where('id', $id)->update([
            'action' => requestExport,
        ]);
    }
    
    public function doExport($id)
    {
        static::where('id', $id)->update([
            'action' => requestExported,
        ]);
    }

    public function doReturn($id)
    {
        static::where('id', $id)->update([
            'status' => returned,
        ]);
    } 
    
    public function updateAction($id, $action)
    {
        static::where('id', $id)->update([
            'action' => $action,
        ]);
    }

    public function updateStatus($id, $status)
    {
        static::where('id', $id)->update([
            'status' => $status,
        ]);
    }
  
   
}
