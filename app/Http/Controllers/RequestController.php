<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestStoreRequest;
use App\Http\Requests\RequestUpdateRequest;
use App\Models\ProductModel;
use App\Models\RequestModel;
use App\Models\HistoryRequestModel;
use Illuminate\Http\Request;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $request;
    private $product;
    private $historyRequest;
    public function __construct(RequestModel $request, ProductModel $product,HistoryRequestModel $historyRequest )
    {
        // $this->authorize('auth');
        $this->request = $request;
        $this->product = $product;
        $this->historyRequest = $historyRequest;
    }
    public function index()
    {
        $this->authorize('request.index');
        $requests= $this->request->list();
        return view('admin.requests.index',[
            'title' => 'Danh sách yêu cầu',
            'topTitle' => 'Danh sách yêu cầu ('.$requests->count().')',
            'requests' => $requests
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('is-user');
        $product= $this->product->findBySlug(request()->slug);
        if(empty($product)) {
          return redirect()->route('user.view.product')->with('error', 'Có lỗi vui lòng quay lại sau');
        }
        return view('admin.requests.create', [
            'title' => 'The First One', 
            'topTitle' => 'Đăng ký mượn sản phẩm : '.$product->name,
            'product' =>  $product
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestStoreRequest $request)
    {
        $this->request->createRequest($request);
        return redirect(route('user.view.product'))->with('success', 'Tạo thành công phiếu mượn :' . $request->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $request= $this->request->findById($id);
        if ($request== false) {
            return redirect()->back()->with('error', 'Có lỗi vui lòng quay lại sau');
        }
        $product = $this->product->findById($request->product_id);
        return view ('admin.requests.update', [
            'title' => 'Sửa yêu cầu',
            'request' => $request,
            'product' => $product
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(RequestUpdateRequest $request, $id)
    {
        $this->request->updateRequest($request, $id);
        return redirect()->route('user.list.request')->with('success', 'Sửa thành công yêu cầu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd(123);
    }

    public function delete($id)
    {
        if($this->request->deleteRequest($id)) {
            return redirect()->back()->with('success', 'Đã xóa yêu cầu');
        }
        return redirect()->back()->with('error', 'Có lỗi trong quá trình xóa');
    }
    public function userList()
    {
        $this->authorize('is-user');
        $listRequsets = $this->request->UserListRequest();
        return view('admin.requests.user_list_request', [
            'title' => 'Danh sách yêu cầu',
            'topTitle' => 'Yêu cầu của tôi',
            'listRequsets' => $listRequsets
        ]);
    }
    public function requestSolved(Request $request)
    {
        $this->authorize('is-admin');
        $this->request->requestSolved($request->id);
        return redirect()->back()->with('success','Bạn đã chấp nhận yêu cầu mượn sản phẩm');
    }

    public function requestExport(Request $request)
    {
        $this->request->requestExport($request->id);
        return redirect()->back()->with('success','Đã gửi yêu cầu xuất sản phẩm thành công');
    }

    public function doExport(Request $request)
    {
        $this->authorize('is-admin');
        $this->request->doExport($request->id);
        $this->historyRequest->createHistory($request->id, historyRequestAgree);
        $this->product->updateStatus($this->request->find($request->id)->product_id, borrow);
        return redirect()->back()->with('success','Sản phẩm đã được xuất khỏi kho');
    }
    public function requestRefuse(Request $request)
    {
        $this->authorize('is-admin');
        $this->request->requestRefuse($request->id);
        $this->historyRequest->createHistory($request->id, historyRequestDeny);
        return redirect()->back()->with('success','Đã từ chối yêu cầu');
    }

    public function requestReturn(Request $request)
    {
        $this->request->updateAction($request->id, requestReturn);
        return redirect()->back()->with('success','Bạn đã yêu cầu trả sản phẩm');
    }

    public function doReturn(Request $request)
    {
        // $this->authorize('is-admin');
        $this->request->updateStatus($request->id, returned);
        $this->request->updateAction($request->id, requestDefault);
        $this->product->updateStatus($this->request->find($request->id)->product_id, warehouse);
        $this->historyRequest->updateStatus($request->id, historyRequestReturn);
        return redirect()->back()->with('success','Đã chấp nhận yêu cầu trả sản phẩm');
    }
}
