<?php

namespace App\Http\Controllers;

use App\Http\Requests\DepartmentStoreRequest;
use App\Http\Requests\DepartmentUpdateRequest;
use App\Models\DepartmentModel;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $department;
    public function __construct(DepartmentModel $department)
    {
        $this->department = $department;
    }
    public function index()
    {
        $this->authorize('department.index');
        $departments = $this->department->all();
        return view('admin.departments.index',[
            'title' => 'List Department',
            'topTitle' => 'Danh sách phòng ban ('.$departments->count().')',
            'departments' => $departments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('department.create');
        return view('admin.departments.create',[
            'title' => 'Create Department',
            'topTitle' => 'Tạo Phòng ban',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentStoreRequest $request)
    {
        $this->authorize('department.create');
        $this->department->create([
            'name' => $request->name,
        ]);
        return redirect()->back()->with('success', 'Thêm thành công Phòng ban :'. $request->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('department.update');
        $department = $this->department->find($id);
        return view('admin.departments.update',[
            'title' => 'Edit Department',
            'topTitle' => 'Sửa phòng ban',
            'department' => $department
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentUpdateRequest $request, $id)
    {
        $this->authorize('department.update');
        $department = $this->department->find($id);
        $department->update([
            'name' => $request->name,
        ]);
        return redirect(route('departments.index'))->with('success', 'Sửa thành công danh mục:'. $department->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('department.delete');
        if($this->department->del($id)) {
            return redirect()->back()->with('success', 'Đã xóa thành công');
        }
        return redirect()->back()->with('error', 'Có lỗi, Vui lòng liên hệ với quan trị viên');
    }
}
