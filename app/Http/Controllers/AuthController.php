<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckMailRequest;
use App\Http\Requests\FirstLoginRequest;
use App\Http\Requests\LoginRequest;
use App\Models\ChangePasswordModel;
use App\Models\UserModel;
use App\Models\CategoryModel;
use App\Models\ProductModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{
    private $user;
    private $reset;
    private $category;
    private $product;
    public function __construct(UserModel $user, ChangePasswordModel $changePassword, CategoryModel $category, ProductModel $product) 
    {
        $this->user= $user;
        $this->category= $category;
        $this->product= $product;
        $this->reset= $changePassword;
    }

    public function index()
    {
        $categories = $this->category->list(); 
        $products = $this->product->list(); 
        $data = $this->category->chartJs($categories);
        
        return view('admin.auth.dashboard',[
            'title' => 'Trang chủ',
            'products' => $products,
            'data' => $data,
        ]);
    }

    public function login()
    {
        if(Auth::check()) {
            return redirect(route('dashboard'));
        }
        return view('admin.auth.login',[
            'title' => 'Login'
        ]);
    }

    public function logon(LoginRequest $request)
    {
        if (auth::attempt([
            'user' => $request->user,
            'password' => $request->password,
            'type' => $request->role,
        ])) {
            return redirect()->route('dashboard')->with('success, Đăng nhập thành công');
        }
        return redirect()->route('login')->with('error', 'Tài khoản hoặc password không đúng');
    }

    public function firstLogin()
    {
        return view('admin.auth.first_login');
    }

    public function firstLogon(FirstLoginRequest $request)
    {
        $this->user->find(Auth::id())->update([
            'password' => Hash::make($request->password),
            'first_login' => '1'
            ]);
            return redirect(route('dashboard'));
    }
    
    public function regiter()
    {
        return view('admin.auth.regiter',[
            'title' => 'Regiter'
        ]);
    }
    
    public function forgotPassword()
    {
        return view('admin.auth.forgot_password',[
            'title' => 'Reset Password'
        ]);
    }

    public function changePassword(Request $request)
    {
        $item = $this->reset->findByToken($request->token); 
        if($item){
          return view('admin.auth.change_password');
        } 
        return view('errors.404');
    }

    public function doChangePassword(FirstLoginRequest $request)
    {
        $this->reset->doChangPassword($request);
        return redirect(route('login'))->with('success','Thay đổi mật khẩu thành công');
    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('login'));
    }

    public function checkEmail(CheckMailRequest $request, UserModel $user)
    {
        $user = $this->user->checkEmail($request->email);
        if($user) {
            $token= $this->reset->createtoken($user);
            $this->user->sendMail($user, $token);
            return redirect()->back()->with('success', 'Thư đổi password đã được gửi vào mail của bạn');
        }
        return redirect()->back()->with('error', 'Email không tồn tại');
    }
}
