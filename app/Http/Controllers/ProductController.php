<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\CategoryModel;
use App\Models\ProductModel;
use App\Helper\Image;
use App\Http\Requests\ProductImportRequest;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $product;
    private $category;
    public function __construct(ProductModel $product, CategoryModel $category)
    {
        $this->product = $product;
        $this->category = $category;
    }

    public function index()
    {
        $this->authorize('product.index');
        $products= $this->product->list();
        return view('admin.products.index',[
            'title' => 'List product',
            'topTitle' => 'Danh sách sản phẩm : ('.$products->count().')',
            'products' => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('product.create');
        $categories = $this->category->all();
        return view('admin.products.create',[
            'title' => 'Create product',
            'topTitile' => 'Thêm sản phẩm',
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        $this->authorize('product.create');
        $this->product->createProduct( $request);
        return redirect()->back()->with('success','Thêm thành công sản phẩm : '.$request->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->find($id);
        return view('admin.products.show',[
            'title' => 'Chi tiết sản phẩm',
            'topTitle' => 'Sản phẩm : '.$product->name,
            'product' => $product
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('product.update');
        $product = $this->product->find($id);
        $categories = $this->category->all();
        return view('admin.products.update',[
            'title' => 'Edit product',
            'topTitile' => 'Sửa sản phẩm :'. $product->name,
            'categories' => $categories,
            'product' => $product
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, $id)
    { 
        $this->authorize('product.update');
        $this->product->updateProduct($request, $id);
        return redirect()->back()->with('success','Sửa thành công sản phẩm : '.$request->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('product.delete');
       if ($this->product->deleteProduct($id)) {
            return redirect(route('products.index'))->with('success', 'Xóa thành công');
       }
       return redirect()->back()->with('error', 'Có lỗi trang quá trình xóa, vui lòng quay lại sau');
    }

    public function userView()
    {
        $products= $this->product->list();
        return view('admin.products.user_view', [
            'title' => 'Danh sách vật phẩm',
            'topTitle' => 'Danh sách vật phẩm ('.$products->count().')',
            'products' => $products
        ]);
    }
    
    public function updateDescription ($id)
    {
        $this->authorize('is-admin');
        $product = $this->product->find($id);
        return view('admin.products.update_import',[
            'title' => 'Edit product',
            'topTitile' => 'Cập nhật tình trạng :'. $product->name,
            'product' => $product
        ]);

    }
}
