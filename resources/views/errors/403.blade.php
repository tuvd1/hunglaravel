
<!DOCTYPE html>
<html lang="en" class="h-100">


<!-- Mirrored from demo.themefisher.com/focus/page-error-403.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Apr 2022 18:14:36 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>The First One</title>
    <!-- Favicon icon -->
    <link rel="icon" type="{{ asset('adminlte/image/png') }}" sizes="16x16" href="{{ asset('adminlte/images/favicon.png') }}">
    <link href="{{ asset('adminlte/css/style.css') }}" rel="stylesheet">
    
</head>

<body class="h-100">
    <div class="authincation h-100">
        <div class="container-fluid h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-5">
                    <div class="form-input-content text-center">
                        <div class="mb-5">
                            <a class="btn btn-primary" href="{{ route('dashboard') }}">Back to dashboard</a>
                        </div>
                        <h1 class="error-text  font-weight-bold">403</h1>
                        <h4 class="mt-4"><i class="fa fa-times-circle text-danger"></i> Forbidden Error!</h4>
                        <p>You do not have permission to view this resource.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>


<!-- Mirrored from demo.themefisher.com/focus/page-error-403.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Apr 2022 18:14:36 GMT -->
</html>