@extends('admin.layouts.main')
@section('css')
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('categories.index') }}">Danh sách danh mục</a></li>
    <li class="breadcrumb-item active">Tạo danh mục </li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @include('admin.layouts.alert')
        <form action="{{ route('categories.store') }}" method="post">
            @csrf
            <div class="form-group">
                @include('admin.components.label_form', [
                    'label' => 'Tên danh mục ',
                    'name' => 'name',
                    'placeholder' => 'Mời bạn nhập tên danh mục ...',
                    'value' => old('name'),
                ])
            </div>
            <button type="submit" class="btn btn-sm btn-primary">Thêm Danh mục</button>
        </form>
    </div>
@endsection
