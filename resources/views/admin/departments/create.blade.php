@extends('admin.layouts.main')
@section('css')
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('departments.index') }}">Danh sách phòng ban</a></li>
    <li class="breadcrumb-item active">Tạo phòng ban </li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @include('admin.layouts.alert')
        <form action="{{ route('departments.store') }}" method="post">
            @csrf
            <div class="form-group">
                @include('admin.components.label_form', [
                    'label' => 'Tên Phòng Ban',
                    'name' => 'name',
                    'placeholder' => 'Mời bạn nhập tên Phòng ban ...',
                    'value' => old('name'),
                ])
            </div>
            <button type="submit" class="btn btn-sm btn-primary">Thêm Phòng Ban</button>
        </form>
    </div>
@endsection
