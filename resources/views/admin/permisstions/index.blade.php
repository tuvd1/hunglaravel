@extends('admin.layouts.main')
@section('css')
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('permisstions.index') }}">Permisstions</a></li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @include('admin.layouts.alert')
        <table class="table table-light table-hover">
            <thead class="thead-light">
                <tr>
                    <th width=5%>STT</th>
                    <th>Tên Permisstion</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($permisstions as $key => $permisstion)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $permisstion->name }}</td>
                        {{-- <td>
                            <div class="row">
                                <form action="{{ route('permisstions.destroy', $permisstion->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                        <i class="fa fa-trash"></i></button>
                                </form>
                            </div>
                        </td> --}}
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
@endsection
