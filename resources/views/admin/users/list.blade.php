@extends('admin.layouts.main')
@section('css')
    <link rel="stylesheet" href="{{ asset('adminlte\style\user\select2.min.css') }}">
@endsection
@section('js')
    <script src="{{ asset('adminlte\style\user\user.js') }}"></script>
    <script src="{{ asset('adminlte\style\user\select2.min.js') }}"></script>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('users.index') }}">Danh sách hân viên</a></li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @include('admin.layouts.alert')
        <table class="table table-light table-hover">
            <thead class="thead-light">
                <tr>
                    <th width=5%>STT</th>
                    <th>Tên Thành viên</th>
                    <th>User</th>
                    <th>Phòng ban</th>
                    <th>Đang mượn</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $key => $user)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->user }}</td>
                        <td><a href="?department={{ $user->department_id }}">{{ $user->department->name ?? '' }}</a></td>
                        <td>{{ $user->requsets->where('status', requestSolved)->whereIn('action', [requestExported, requestReturn])->count() }}</td>
                        <td>
                            <div class="row">

                                @can('user.index')
                                    <a href="{{ route('users.info', $user->id) }}" class="btn button btn-warning"><i class="far fa-info"></i></a> &ensp;
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div>{!! $users->appends(request()->all())->links() !!}</div>
    </div>
@endsection
