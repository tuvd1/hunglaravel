<form action="{{ $route ?? route('users.update', $user->id) }}" method="Post" enctype="multipart/form-data">
    @csrf
    @method($method ?? 'post')
    <div class="form-group">
        @include('admin.components.input', [
            'name' => 'id',
            'type' => 'hidden',
            'value' => $user->id ?? '',
        ])
        @include('admin.components.label_form', [
            'label' => 'Tên đầy đủ',
            'name' => 'name',
            'placeholder' => 'Mời bạn nhập tên đầy đủ ...',
            'value' => $user->name ?? old('name'),
        ])
        @include('admin.components.label_form', [
            'label' => 'Tài khoản',
            'name' => 'user',
            'placeholder' => 'Mời bạn nhập tên tài khoản ...',
            'atribute' => isset($user) ? 'readonly' : '',
            'value' => $user->user ?? old('user'),
        ])
        @include('admin.components.label_form', [
            'label' => 'Email',
            'name' => 'email',
            'type' => 'email',
            'placeholder' => 'Mời bạn nhập email...',
            'value' => $user->email ?? old('email'),
        ])
        <div class="form-group">
            <label for="">Giới tính</label>
            <select name="gender" id="" class="form-control select2-role">
                <option {{ isset($user) && $user->gender == genderMafe ? 'selected' : '' }} value="0">Nam</option>
                <option {{ isset($user) && $user->gender == genderFemale ? 'selected' : '' }} value="1">Nữ
                </option>
            </select>
        </div>
        @include('admin.components.label_form', [
            'label' => 'Số điện thoại',
            'name' => 'phone',
            'placeholder' => 'Mời bạn nhập số điện thoại...',
            'value' => $user->phone ?? old('phone'),
        ])
        @include('admin.components.label_form', [
            'label' => 'Địa chỉ',
            'name' => 'address',
            'placeholder' => 'Mời bạn nhập địa chỉ...',
            'value' => $user->address ?? old('address'),
        ])
        @include('admin.components.select_form', [
            'label' => 'Phòng ban',
            'name' => 'department_id',
            'data' => $departments,
            'id' => $department_id ?? '',
            'class' => "form-control select2-role"
        ])

        <div class="form-group">
            <label for="">Type</label>
            <select name="type" id="" class="form-control select2-role">
                <option {{ isset($user) && $user->type == typeAdmin ? 'selected' : '' }} value="0">Hệ Thống
                </option>
                <option {{ isset($user) && $user->type == typeUser ? 'selected' : '' }} value="1">User</option>
            </select>
        </div>


        <div class="form-group">
            <label for="role">Vai trò</label>
            <select class="form-control select2-role" name="role[]" id="role" multiple>
                @foreach ($roles as $role)
                    <option value="{{ $role->id }}"  {{ isset($user) && $user->roles->contains('id',$role->id)? "selected": ""}}>{{ $role->name }}</option>
                @endforeach
            </select>
        </div>

        @include('admin.components.label_form', [
            'label' => 'Mật khẩu',
            'name' => 'password',
            'type' => 'password',
            'placeholder' => 'Mật khẩu...',
        ])

        @include('admin.components.label_form', [
            'label' => 'Nhập lại mật khẩu',
            'name' => 'password',
            'type' => 'confirm_password',
            'placeholder' => 'Nhập lại mật khẩu...',
        ])

        <div class="form-group">
            <button type="submit" class="btn btn-sm btn-primary">{{ $button }}</button>
        </div>
    </div>
</form>
