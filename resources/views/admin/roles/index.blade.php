@extends('admin.layouts.main')
@section('css')
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Danh sách Role</a></li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @can('role.create')
        <a href="{{ route('roles.create') }}" class="btn button btn-sm btn-primary" style="float: right">Thêm Role</a>
        @endcan
        <div class="col-10">@include('admin.layouts.alert')</div>
        <table class="table table-light table-hover">
            <thead class="thead-light">
                <tr>
                    <th width=5%>STT</th>
                    <th>Tên Roles</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roles as $key => $role)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $role->name }}</td>
                        <td>
                            <div class="row">
                                @can('role.update')
                                <a href="{{ route('roles.edit', $role->id) }}" class="btn button btn-warning"><i
                                    class="fa fa-edit"></i></a> &ensp;
                                @endcan
                                @can('role.delete')
                                <form action="{{ route('roles.destroy', $role->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn button btn-danger" onclick="return confirm('Are you sure?')">
                                        <i class="fa fa-trash"></i></button>
                                </form>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
