@extends('admin.layouts.main')
@section('css')
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('user.list.request') }}">List Requests</a></li>
    <li class="breadcrumb-item active">Create Request</li>
@endsection
@section('content')
<div class="col-12 box-bety">
    @include('admin.layouts.alert')
    @include('admin.requests.form', [
        'product' => $product,
        'button' => 'Tạo Yêu cầu',
        'route' => route('requests.store'),
    ])
</div>
@endsection
