@extends('admin.layouts.main')
@section('css')
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{ route('requests.index') }}">Danh sách yêu cầu</a></li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @include('admin.layouts.alert')
        @if ($requests->count() > 0)
            <table class="table table-light table-hover">
                <thead class="thead-light">
                    <tr>
                        <th width=5%>STT</th>
                        <th>Tên sản phẩm</th>
                        <th>Mã sản phẩm</th>
                        <th>Ngày mượn</th>
                        <th>Ngày trả</th>
                        <th>Người mượn</th>
                        <th>Người xác nhận</th>
                        <th>Tình trạng</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($requests as $key => $request)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $request->product->name }}</td>
                            <td>{{ $request->product->code }}</td>
                            <td>{{ $request->brorrowed_date }}</td>
                            <td>{{ $request->date_off }}</td>
                            <td>{{ $request->user->name }}</td>
                            <td>{{ $request->history->user->name ?? '' }}</td>
                            <td>
                                @if ($request->status == returned)
                                    <span><a class="badge button badge-dark" href="?status={{ requestReturn }}">Đã trả
                                            hàng</a></span>
                                @elseif ($request->status == requestPending)
                                    <span><a class="badge button badge-secondary" href="?status={{ requestPending }}">Đang
                                            chờ</a></span>
                                @elseif ($request->action == requestExported || $request->action == requestReturn)
                                    <span><a class="badge button badge-outline-dark"
                                            href="?action={{ requestExported }}">Đã
                                            xuất </a></span>
                                @elseif($request->status == requestSolved)
                                    <span><a class="badge button badge-success" href="?status={{ requestSolved }}">Đã
                                            duyệt</a></span>
                                @elseif($request->status == requestRefuse)
                                    <span><a class="badge button badge-danger" href="?status={{ requestRefuse }}">Từ
                                            chối</a></span>
                                @endif
                            <td>
                                @if ($request->status == requestPending)
                                    <div class="row">
                                        @if ($request->product->status == warehouse)
                                            <form action="{{ route('request.solved') }}" method="post">
                                                @csrf
                                                @method('put')
                                                <input type="hidden" name="id" value="{{ $request->id }}">
                                                <button type="submit" class="button btn btn-sm btn-success"
                                                    onclick="return confirm('Bạn có chắc chắn đồng ý cho mượn sản phẩm?')">
                                                    <i class="fa fa-check"></i></button>
                                            </form>&ensp;
                                        @else
                                            <span class="button badge badge-warning"><i
                                                    class="fal fa-exclamation-circle"></i></span> &ensp;
                                        @endif

                                        <form action="{{ route('request.refuse') }}" method="post">
                                            @csrf
                                            @method('put')
                                            <input type="hidden" name="id" value="{{ $request->id }}">
                                            <button type="submit" class="button btn btn-sm btn-danger"
                                                onclick="return confirm('Bạn có chắc chắn từ chối yêu cầu?')">
                                                <i class="fa fa-times"></i></button>
                                        </form>
                                    </div>
                                @elseif($request->status == requestSolved && $request->action == requestExport)
                                    <div class="row">
                                        @if ($request->product->status == warehouse)
                                            <form action="{{ route('do.export') }}" method="post">
                                                @csrf
                                                @method('put')
                                                <input type="hidden" name="id" value="{{ $request->id }}">
                                                <button type="submit" class="button badge badge-primary"
                                                    onclick="return confirm('Bạn có chắc chắn export sản phẩm?')">
                                                    <i class="fal fa-file-export"></i></button>
                                            </form> &ensp;
                                        @else
                                            <span class="btn button btn-sm btn-warning"><i
                                                    class="fal fa-exclamation-circle"></i></span>&ensp;
                                        @endif
                                    </div>
                                @elseif($request->action == requestReturn && $request->status != returned)
                                    <form action="{{ route('do.return') }}" method="post">
                                        @csrf
                                        @method('put')
                                        <input type="hidden" name="id" value="{{ $request->id }}">
                                        <button type="submit" class="button btn btn-sm btn-outline-dark"
                                            onclick="return confirm('Đồng ý nhận sản phẩm?')">
                                        Import</button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div >{!! $requests->appends(request()->all())->links() !!}</div>
        @else
            <div style="text-align:center">Không tìm thấy yêu cầu nào...</div>
        @endif
    </div>
@endsection
