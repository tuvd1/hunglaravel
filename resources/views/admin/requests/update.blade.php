@extends('admin.layouts.main')
@section('css')
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('user.list.request') }}">Danh sách yêu cầu</a></li>
    <li class="breadcrumb-item active">Sửa Yêu cầu</li>
@endsection
@section('content')
<div class="col-12 box-bety">
    @include('admin.layouts.alert')
    @include('admin.requests.form', [
        'product' => $product,
        'button' => 'Sửa yêu cầu',
        'method' => 'put'
    ])
</div>
@endsection
