@extends('admin.layouts.main')
@section('css')
    <style>
        th img {
            box-shadow: rgb(0 0 0 / 25%) 0px 14px 28px, rgb(0 0 0 / 22%) 0px 10px 10px;
            width: 250px;
            height: 150px;
            object-fit: cover;
            border-radius: 10px; 
        }
    </style>
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Danh sách sản phẩm</a></li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @can('product.create')
            <a href="{{ route('products.create') }}" class="btn button btn-sm btn-primary" style="float: right">Thêm sản phẩm</a>
        @endcan
        @include('admin.layouts.alert')
        <table class="table table-light table-hover">
            <thead class="thead-light">
                <tr>
                    <th width=5%>STT</th>
                    <th>Tên sản phẩm</th>
                    <th>Mã sản phẩm</th>
                    <th>Danh mục</th>
                    <th>Ảnh</th>
                    <th>Tình trạng</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $key => $product)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->code }}</td>
                        <td><a href="?category={{ $product->category->id }}">{{ $product->category->name ?? '' }}</a></td>
                        <th><img height="100px;" src="{{ asset('uploads/product/' . $product->image) }}"
                                alt="image not found"></th>
                        <td>{{ $product->status == warehouse ? 'Trong kho' : 'Đang mượn' }}</td>
                        <td>
                            <div class="row">
                                @can('product.update')
                                    <a href="{{ route('products.edit', $product->id) }}" class=" button btn btn-warning"><i
                                            class="fa fa-edit"></i></a> &ensp;
                                @endcan

                                @can('product.delete')
                                    <form action="{{ route('products.destroy', $product->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class=" button btn btn-danger" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-trash"></i></button>
                                    </form>
                                @endcan


                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div >{!! $products->appends(request()->all())->links() !!}</div>
    </div>
@endsection
