@extends('admin.layouts.main')
@section('css')
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('products.index') }}">Danh sách sản phẩm</a></li>
    <li class="breadcrumb-item active">Sửa sản phẩm</li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @include('admin.layouts.alert')
        @include('admin.products.form', [
            'lable' => 'Phòng ban',
            'categories' => $categories,
            'button' => 'Thêm sản phẩm',
            'product' => $product,
            'button' => 'Sửa Sản phẩm',
            'method' => 'put',
        ])
    </div>
@endsection
