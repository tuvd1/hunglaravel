<form action="{{ $route ?? route('products.update', $product->id) }}" method="post" enctype="multipart/form-data">
    @csrf
    @method($method ?? 'post')

    @include('admin.components.input', [
            'name' => 'id',
            'type' => 'hidden',
            'value' => $product->id ?? '',
        ])
    @include('admin.components.label_form', [
        'label' => 'Tên Sản Phẩm',
        'name' => 'name',
        'placeholder' => 'Mời bạn nhập tên Sản phẩm ...',
        'value' => $product->name ?? old('name'),
    ])

    @include('admin.components.label_form', [
        'label' => 'Mã sản phẩm',
        'name' => 'code',
        'placeholder' => 'Mời bạn nhập tên mã sản phẩm ...',
        'value' => $product->code ?? old('code'),
    ])

    <div class="form-group">
        <label for="">Danh mục</label>
        <select class="form-control" name="category_id" id="">
            @foreach ($categories as $category)
                <option value="{{ $category->id }}" {{ isset($product) && $product->category_id == $category->id ? "selected" : ( old('category_id') == $category->id ? "selected" : "") }}>{{ $category->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="">Tình trạng</label>
        <select class="form-control" name="status" id="">
            <option {{ isset($product) && $product->status == warehouse ? "selected" : ( old('status') == warehouse ? "selected" : "") }} value="0">Trong Kho</option>
            <option {{ isset($product) && $product->status == borrow ? "selected" : ( old('status') == borrow ? "selected" : "")  }} value="1">Đang mượn</option>
        </select>
    </div>

   

    <div class="form-group">
        <label for="note">Ghi chú</label>
        <textarea name="note" class="form-control" id="note" cols="30" rows="10" placeholder="Mời nhập ghi chú...">{{  $product ->note ?? old('note') }}</textarea>
    </div>

    @include('admin.components.label_form', [
        'label' => 'Ảnh sản phẩm',
        'name' => 'image',
        'type' => 'file',
        'class' => 'form-control-file',
    ])

    @if (isset($product))
    <img height="200px;" src="{{ asset('uploads/product/'.$product->image) }}" alt="image not found">
    @endif
    <hr>
    <div class="form-group">
        <button type="submit" class="btn btn-sm btn-primary">{{ $button ?? "Thêm sản phẩm" }}</button>
    </div>

    
</form>