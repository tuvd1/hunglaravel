@extends('admin.layouts.main')
@section('css')
    <link rel="stylesheet" href="{{ asset('adminlte\style\reuquest\request.css') }}">
@endsection
@section('js')
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('user.view.product') }}">Danh sách sản phẩm</a></li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @include('admin.layouts.alert')
        <div class="row">
            @foreach ($products as $product)
                <div class="col-xl-3 col-xxl-4 col-lg-4 col-sm-6 col-xs-12 ">
                    <div class="card mb-3 form-item">
                        <img class="card-img-top img-fluid" style="width:100% ;height:200px"
                            src="{{ asset('uploads/product/' . $product->image) }}" alt="Card image cap">
                          
                        <div class="card-header">
                            <h5 class="card-title">{{ $product->name }}</h5>
                            <a href="?category={{ $product->category_id }} ">{{ $product->category->name ?? ""}}</a>
                        </div>
                        <div class="card-body">
                            <p><b> Mã sản phẩm: </b><span class="card-text">{{ $product->code }}</span></p>
                            <p class="card-text"> <b>Tình trạng: </b> {{ $product->note }}</p>
                            <p class="card-text text-dark">
                                <span><a class="butuon badge badge-warning" href="{{ route('products.show', $product->id) }}">Chi
                                        tiết</a></span>
                                <span
                                    class="badge badge-{{ $product->status == warehouse ? 'success' : 'danger' }}">{{ $product->status == warehouse ? 'Trong kho' : 'Đã mượn' }}</span>
                                @if ($product->status == warehouse)
                                    <span><a class="butuon badge btn-outline-primary badge-outline-primary"
                                            href="{{ route('requests.create') . "?slug=$product->slug" }}">Yêu cầu mượn
                                            </a></span>
                                @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div >{!! $products->appends(request()->all())->links() !!}</div>
    </div>
@endsection
