<!DOCTYPE html>
<html lang="en" class="h-100">


<!-- Mirrored from demo.themefisher.com/focus/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Apr 2022 18:12:45 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Đăng nhập hệ thống</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <link href="{{ asset('adminlte/css/style.css') }}" rel="stylesheet">

</head>

<body class="h-100">
    <div class="authincation h-100">
        <div class="container-fluid h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
                                    <h4 class="text-center mb-4">
                                        @if (isset(request()->admin))
                                            <h3>Đăng nhập hệ thống</h3>
                                        @else
                                            <h3> Đăng nhập User</h3>
                                        @endif
                                    </h4>
                                    <form action="{{ route('logon') }}" method="POST">
                                        @csrf
                                        @include('admin.layouts.alert')
                                        <div class="form-group">
                                            <label><strong>User</strong></label>
                                            <input type="text" class="form-control" name="user"
                                                value="{{ old('user') }}" placeholder="Mời nhập User...">
                                        </div>
                                        <div class="form-group">
                                            <label><strong>Password</strong></label>
                                            <input type="password" class="form-control" name="password" value=""
                                                placeholder="Mời nhập Password...">
                                        </div>
                                        <div class="" style="float: right">
                                            @if (isset(request()->admin))
                                                <input type="hidden" name="role" value="0">
                                                <a href="{{ route('login') }}">Đăng nhập với tài khoản User</a>
                                            @else
                                                <input type="hidden" name="role" value="1">
                                                <a href="?admin=0">Đăng nhập với tài khoản hệ thống</a>
                                            @endif
                                        </div>
                                        <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                            <div class="form-group">
                                                <a href="{{ route('forgot.password') }}">Quên mật khẩu</a>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary btn-block">Đăng nhập</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src=" {{ asset('adminlte/vendor/global/global.min.js') }} "></script>
    <script src=" {{ asset('adminlte/js/quixnav-init.js') }} "></script>
    <script src=" {{ asset('adminlte/js/custom.min.js') }} "></script>

</body>


<!-- Mirrored from demo.themefisher.com/focus/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Apr 2022 18:12:46 GMT -->

</html>
