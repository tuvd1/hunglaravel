<!DOCTYPE html>
<html lang="en" class="h-100">


<!-- Mirrored from demo.themefisher.com/focus/page-register.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Apr 2022 18:14:36 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>First Login</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <link href="{{ asset('adminlte/css/style.css') }}" rel="stylesheet">

</head>

<body class="h-100">
    <div class="authincation h-100">
        <div class="container-fluid h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
                                    <h4 class="text-center mb-4">First Login</h4>
                                    <form action="{{ route('first.logon') }}" method="post">
                                        @csrf
                                        @method('put')
                                        @include('admin.components.label_form', [
                                            'label' => 'Password',
                                            'name' => 'password',
                                            'placeholder' => 'Mời bạn nhập new password ...',
                                            'type' => 'password',
                                        ])
                                        @include('admin.components.label_form', [
                                            'label' => 'Confirm Password',
                                            'name' => 'confirm_password',
                                            'placeholder' => 'Mời bạn nhập confirm-password ...',
                                            'value' => old('email'),
                                            'type' => 'password',
                                        ])
                                        <div class="text-center mt-4">
                                            <button type="submit" class="btn btn-primary btn-block">Thay đổi mật khẩu</button>
                                        </div>
                                    </form>
                                    <div class="new-account mt-3">
                                        <p> <a class="text-primary" href="{{ route('logout') }}">Đăng xuất</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src=" {{ asset('adminlte/vendor/global/global.min.js') }} "></script>
    <script src=" {{ asset('adminlte/js/quixnav-init.js') }} "></script>
    <script src=" {{ asset('adminlte/js/custom.min.js') }} "></script>
    <!--endRemoveIf(production)-->
</body>


<!-- Mirrored from demo.themefisher.com/focus/page-register.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Apr 2022 18:14:36 GMT -->

</html>
