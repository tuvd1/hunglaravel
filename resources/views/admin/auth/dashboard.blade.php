@extends('admin.layouts.main')
@section('css')
    <style>

    </style>
@endsection
@section('js')
    <script src="{{ asset('adminlte\vendor\chart.js\Chart.bundle.min.js') }} "></script>
    <script src="{{ asset('adminlte\vendor\chartist\js\chartist.min.js') }} "></script>
    <script src="{{ asset('adminlte\vendor/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js') }}"></script>
    <script src="{{ asset('adminlte\js/plugins-init/chartist-init.js') }}"></script>
    <script>
        var _ydata = JSON.parse('{!! json_encode($data["names"]) !!}');
        var _xdata = JSON.parse('{!! json_encode($data["count"]) !!}');
        var _xdata1 = JSON.parse('{!! json_encode($data["count1"]) !!}');
    </script>
    <script>
        new Chartist.Bar('#stacked-bar-chart', {
            labels: _ydata,
            series: [
                _xdata,
                _xdata1
            ]
        }, {
            stackBars: true,
            axisY: {
                labelInterpolationFnc: function(value) {
                    return (value);
                }
            },
            plugins: [
                Chartist.plugins.tooltip()
            ]
        }).on('draw', function(data) {
            if (data.type === 'bar') {
                data.element.attr({
                    style: 'stroke-width: 30px'
                });
            }
        });
    </script>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
@endsection
@section('content')
    <div class="col-12 box-bety">
        @include('admin.layouts.alert')
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-lg-11 col-sm-11 " style="margin: 0 auto;">
                        <div class="col-12"
                                style="margin: 0 auto; box-shadow: rgb(0 0 0 / 24%) 0px 3px 8px; color: black; padding : 20px;">
                                <p>Tổng số sản phẩm: {{ $products->count() }}</p>
                                <p>Trong kho : {{ $products->where('status', warehouse)->count() }}</p>
                                <p>Đã xuất : {{ $products->where('status', borrow)->count() }}</p>
                            </div>
                        <div class="card " style="box-shadow: rgb(0 0 0 / 35%) 0px 5px 15px; margin-top: 10px">
                            <div class="card-header">
                                <h4 class="card-title">Biểu đồ quản lý sản phẩm</h4>
                            </div>
                            <div class="card-body">
                                <div id="stacked-bar-chart" class="ct-chart ct-golden-section"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12" style="text-align:center">
                        <span class="btn btn-sm btn-danger"><b>Đang mượn</b></span>
                        <span class="btn btn-sm" style="background-color: #01aab3; color:rgb(248, 244, 244)"><b>Trong
                                kho</b></span>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
@endsection
