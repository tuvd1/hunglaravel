<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Manager</title>
</head>

<body>
    <div>
        Chào bạn {{ $user->name }} Bạn đã yêu cầu thay đổi mật khẩu, click vào link bên dưới để thay đổi password:
        <a href="https://hungbt.dev/changePassword?token={{ $token }}">Click here</a>
    </div>
</body>

</html>
