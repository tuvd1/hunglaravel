     <div class="form-group">
         <label for="" class="card-title">{{ $label ?? 'Label' }}</label>
         <input type="{{ $type ?? 'text' }}" {{ $atribute ?? "" }}  class="{{ $class ?? 'form-control' }}"  value="{{ $value ?? '' }}"
             name="{{ $name ?? '' }}" placeholder="{{ $placeholder ?? '' }}">
         @error($name)
             <span class=" text-danger">{{ $message }}</span>
         @enderror
     </div>
