<div class="quixnav">
    <div class="quixnav-scroll">
        <ul class="metismenu" id="menu">
            @can('is-admin')
                <li class="nav-label first">Main Menu</li>
                @can('category.index')
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                                class="fal fa-tachometer-alt-slowest"></i><span class="nav-text">Danh mục</span></a>
                        <ul aria-expanded="false">
                            <li><a href="{{ route('categories.index') }}">Danh sách danh mục</a></li>
                            @can('category.create')
                                <li><a href="{{ route('categories.create') }}">Create Category</a></li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('product.index')
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                                class="fal fa-tachometer-alt-fastest"></i><span class="nav-text">Sản phẩm</span></a>
                        <ul aria-expanded="false">
                            <li><a href="{{ route('products.index') }}">Danh sách sản phẩm </a></li>
                            @can('product.create')
                                <li><a href="{{ route('products.create') }}">Tạo sản phẩm</a></li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="fas fa-user-tie"></i><span class="nav-text">Danh sách nhân viên</span></a>
                    <ul aria-expanded="false">
                        <li><a href="{{ route('users.list') }}">Danh sách </a></li>
                    </ul>
                </li>
                @can('request.index')
                    <li class="nav-label">Yêu cầu</li>
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                                class="fal fa-folder-tree"></i><span class="nav-text">Danh sách yêu cầu</span></a>
                        <ul aria-expanded="false">
                            <li><a href="{{ route('requests.index') }}?status={{ requestPending }}">Chờ phê duyệt</a>


                            <li><a
                                    href="{{ route('requests.index') }}?status={{ requestSolved }}&ex_action={{ requestExported }}&ex_action1={{ returned }} ">Đã
                                    duyệt</a>
                            </li>
                            <li><a
                                    href="{{ route('requests.index') }}?ex_action={{ requestDefault }}&ex_action1={{ requestExport }}   ">Đã
                                    xuất sản
                                    phẩm</a>
                            </li>
                            <li><a href="{{ route('requests.index') }}?status={{ returned }} ">Sản phẩm đã trả</a>
                            </li>
                            <li><a href="{{ route('requests.index') }}?status={{ requestRefuse }}">Đã bị từ chối</a>
                            </li>
                            <li><a href="{{ route('requests.index') }}">Tất cả Yêu cầu </a></li>
                    </li>

                </ul>
                </li>
            @endcan
            @can('user.index')
                <li class="nav-label">Setting</li>
                <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="fal fa-users"></i><span
                            class="nav-text">Tài khoản</span></a>
                    <ul aria-expanded="false">



                        <li><a href="{{ route('users.index') }}">Danh sách tài khoản</a></li>

                        @can('user.create')
                            <li><a href="{{ route('users.create') }}">Tạo tài khoản </a></li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @can('department.index')
                <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="far fa-building"></i><span
                            class="nav-text">Phòng ban</span></a>
                    <ul aria-expanded="false">

                        <li><a href="{{ route('departments.index') }}">Danh sách phòng ban </a></li>

                        @can('department.create')
                            <li><a href="{{ route('departments.create') }}">Tạo phòng ban</a></li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @can('role.index')
                <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="fas fa-user-cog"></i><span
                            class="nav-text">Roles</span></a>
                    <ul aria-expanded="false">
                        <li><a href="{{ route('roles.index') }}">Danh sách Role </a></li>

                        @can('role.create')
                            <li><a href="{{ route('roles.create') }}">Tạo Role</a></li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @can('permisstion.index')
                <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="fas fa-cogs"></i><span
                            class="nav-text">Pemisstions</span></a>
                    <ul aria-expanded="false">



                        <li><a href="{{ route('permisstions.index') }}">Danh sách Permisstion </a></li>

                    </ul>
                </li>
            @endcan
        @endcan


        @can('is-user', typeUser)
            <li class="nav-label">Dashboard</li>
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="icon icon-app-store"></i><span
                        class="nav-text">Người dùng</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('user.view.product') }} ">Danh sách sản phẩm</a></li>
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Yêu cầu của tôi</a>
                        <ul aria-expanded="false">
                            <li><a href="{{ route('user.list.request') }}?status={{ requestPending }}">Yêu cầu đang
                                    chờ</a></li>
                            <li><a
                                    href="{{ route('user.list.request') }}?status={{ requestSolved }}&ex_action={{ requestExported }}&ex_action1={{ returned }}  ">Yêu
                                    cầu đã duyệt</a></li>
                            <li><a
                                    href="{{ route('user.list.request') }}?ex_action={{ requestDefault }}&ex_action1={{ requestExport }}  ">Yêu
                                    cầu đã xuất
                                    sản phẩm</a></li>
                            <li><a href="{{ route('user.list.request') }}?status={{ returned }} ">Yêu cầu đã trả
                                    lại</a></li>
                            <li><a href="{{ route('user.list.request') }}?status={{ requestRefuse }}">Yêu cầu bị từ
                                    chối</a></li>
                            <li><a href="{{ route('user.list.request') }} ">Tất cả yêu cầu</a></li>
                            <li>
                        </ul>
                    </li>
                </ul>
            </li>
        @endcan

        </ul>
    </div>


</div>
