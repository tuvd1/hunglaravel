<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from demo.themefisher.com/focus/chart-chartist.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Apr 2022 18:13:07 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>{{ $title ?? 'Trang chủ' }} </title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('adminlte/images/favicon.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('adminlte/vendor/chartist/css/chartist.min.css') }}">
    <link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('adminlte/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('adminlte/css/my_css.css') }}">
    @yield('css')
</head>

<body>
    <div id="main-wrapper">

        @include('admin.layouts.header')

        @include('admin.layouts.sidebar')

        <div class="content-body" style="min-heag">
            <div class="container-fluid" style="min-height: 700px;">
                <div class="row page-titles mx-0 top-bety">
                    <div class="col-sm-6">
                        <div class="welcome-text">
                            <h4>{{ $topTitle ?? ($title ?? 'Trang chủ') }}</h4>
                        </div>
                    </div>
                    <div class="col-sm-6 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                        <ol class="breadcrumb">
                            @yield('breadcrumb')
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            @yield('content')
                        </div>
                    </div>

                </div>
            </div>
            <!--**********************************
            Content body end
        ***********************************-->


            <!--**********************************
            Footer start
        ***********************************-->
            <div class="footer" style="margin-top:10em">
                <div class="copyright">
                    <p>Copyright © Designed &amp; Developed by <a href="#" target="_blank">Quixkit</a> 2019</p>
                </div>
            </div>
            <!--**********************************
            Footer end
        ***********************************-->

            <!--**********************************
           Support ticket button start
        ***********************************-->

            <!--**********************************
           Support ticket button end
        ***********************************-->


        </div>
        <!--**********************************
        Main wrapper end
    ***********************************-->

        <!--**********************************
        Scripts
    ***********************************-->
        <!-- Required vendors -->
        <script src=" {{ asset('adminlte/vendor/global/global.min.js') }} "></script>
        <script src=" {{ asset('adminlte/js/quixnav-init.js') }} "></script>
        <script src=" {{ asset('adminlte/js/custom.min.js') }} "></script>
        <script src=" {{ asset('fontawesome/js/fontawesome.js') }} "></script>


        <!-- Chart Chartist plugin files -->
        <script src=" {{ asset('adminlte/vendor/chartist/js/chartist.min.js') }} "></script>
        <script src=" {{ asset('adminlte/vendor/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js') }} "></script>
        <script src=" {{ asset('adminlte/js/plugins-init/chartist-init.js') }} "></script>
        @yield('js')
</body>
{{-- {{ asset('adminlte/') }} --}}

<!-- Mirrored from demo.themefisher.com/focus/chart-chartist.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Apr 2022 18:13:09 GMT -->

</html>
