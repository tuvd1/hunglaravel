<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name',191);
            $table->string('user',191)->unique();
            $table->string('email')->unique();
            $table->tinyInteger('gender')->default(0)->comment('1 => nữ, 0 => nam');
            $table->string('phone',10)->NULL;
            $table->string('address',191)->NULL;
            $table->tinyInteger('first_login')->default(0)->comment('0=>chưa đổi , 1=>đã đổi');
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('department_id');
            $table->tinyInteger('type');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
