<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name',191)->nullable();
            $table->string('slug',191)->nullable();
            $table->unsignedBigInteger('category_id');
            $table->string('code',191)->nullable()->unique();
            $table->tinyInteger('status')->default(0)->comment('0 => trong kho, 1 => đã cho mượn');
            $table->text('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
