<?php
return [
    'module' => [
        'categorys',
        'products',
        'requests',
        'roles',
        'users',
        'departments',
        'permisstions',
    ],
    'module_child' => [
        'List',
        'Add',
        'Edit',
        'Delete',
    ]
];